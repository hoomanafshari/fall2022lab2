//Hooman Afshari 2134814
package Application;
import vehicles.Bicycle;
public class BikeStore {
     public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0]= new Bicycle("New",21,25);
        bikes[1]= new Bicycle("Lambo",32,35);
        bikes[2]= new Bicycle("Bixi",34,45);
        bikes[3]= new Bicycle("BMW",40,25);
        
        for (int i=0; i<bikes.length ; i++) {
            System.out.println(bikes[i]);
        }
        
    }
}
